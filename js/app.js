var app = angular.module('NI', ['ui.swiper','ngMaterial','ngAnimate']);


url = "../../cartelera_backend_laravel/public/";


app.controller('cofigCtrlNI', ['$scope', '$http', '$interval','$rootScope', function ($scope, $http, $interval, $location, $rootScope) {

}]);

app.controller('swiperCtrl', function($scope, $http, $interval,$location,$timeout,$rootScope,$sce) {
	
	$scope.demo=false;
	$rootScope.fsala_id =undefined;

	var name='sala';
 	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
	var idsala = regex.exec($location.absUrl());	

	var name1='fingerprint';
 	name1 = name1.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex1 = new RegExp("[\\?&]" + name1 + "=([^&#]*)");
	var fingerprint = regex1.exec($location.absUrl());

	$http.get('../../cartelera_backend_laravel/public/?ruta=iniciasesion&finger='+fingerprint[1]).then(function(data){
    	$scope.sesion=data.data;
		
		
	})	
	
	

	if(idsala && idsala[1]>0){
		$scope.demo=true;
		$scope.sala_demo=idsala[1];
		$scope.url_demo = '../_cs/?sala='+idsala[1];
		$scope.url_demo = $sce.trustAsResourceUrl($scope.url_demo);
	}
	$scope.license_message = '';
	$scope.license_alert = false;
	

	if($location.host() != 'localhost'){
		$http.get('../../cartelera_backend_laravel/public/?ruta=validarfinger&fp='+fingerprint[1]).then(function(data){
			var valor = data.data.true;	
			
	      	
	      		      	
	      	if (valor){	
	      		var finger = data.data.table.fingerprint;	
	      		$rootScope.fsala_id=data.data.table.sala_id;	
	      		
	      		    		
	      	}else{
	      		$scope.license_alert = true;
	      	}

		});

		
	}
	$scope.vista_previa=function(){
		$scope.license_alert = false;

		setTimeout(function(){
			$scope.license_alert = true;
		}, 5000);
	}
	$scope.host = $location.host();
	$scope.port = $location.port();
	if($location.port() != '80'){
		var url = "http://"+$location.host()+':'+$location.port()+"/cartelera_backend_laravel/public/";
	}else{
		var url = "http://"+$location.host()+"/cartelera_backend_laravel/public/";
	}
	
	$scope.id_sala = '';
	$scope.cvnShow=false;
	$scope.cv2Show=false;
	$scope.caShow=false;
	$scope.csShow=false;
	$scope.chShow=false;
	$scope.cdShow=false;
	$scope.ciShow=false;
	$scope.crShow=false;
	$scope.mfShow=false;
	$rootScope.csAct=false;
	$scope.public_act = false;

	$rootScope.primeravez=false;

	$scope.musica_funcional = 0;
	$scope.cartelera_informacion = 0;
	$scope.cartelera_control = 0;
	$scope.catalogo_digital = 0;
	$scope.cartelera_homenaje = 0;

	$http.get(url+'?ruta=api/licencia_info&finger='+fingerprint[1]).then(function(data){
		console.log('100',data.data);
		var licencias = data.data.licencia;
		$scope.cont=0;
		for(var i=0;i<licencias.length;i++){
			
			if(licencias[i].product_id==6 && licencias[i].status=='Active' && licencias[i].descarga==1){
				$rootScope.csAct=true;
				$rootScope.showCS();
				$('body').removeClass('hide');
				$scope.cont++;
			}
			if(licencias[i].product_id==5 && licencias[i].status=='Active' && licencias[i].descarga==1){
				if($rootScope.csAct==false){
					$rootScope.showCH();
				}
				$scope.cartelera_homenaje = 1;
				$('body').removeClass('hide');
				$scope.cont++;
			}
			if(licencias[i].product_id==9 && licencias[i].status=='Active' && licencias[i].descarga==1){
				if($rootScope.csAct==false && $scope.chShow==false){
					$rootScope.showCI();
				}
				$scope.cartelera_informacion = 1;
				$('body').removeClass('hide');
				$scope.cont++;
			}
			if(licencias[i].product_id==33 && licencias[i].status=='Active' && licencias[i].descarga==1){
				if($rootScope.csAct==false && $scope.chShow==false && $scope.ciShow==false){
					$rootScope.showCD();
				}
				$scope.catalogo_digital = 1;
				$('body').removeClass('hide');
				$scope.cont++;
			}
			if(licencias[i].product_id==32 && licencias[i].status=='Active' && licencias[i].descarga==1){
				if($rootScope.csAct==false && $scope.chShow==false && $scope.ciShow==false && $scope.cdShow==false){
					$rootScope.showCR();
				}
				$scope.cartelera_control = 1;
				$('body').removeClass('hide');
				$scope.cont++;
			}
			if(licencias[i].product_id==13 && licencias[i].status=='Active' && licencias[i].descarga==1){
				if($rootScope.csAct==false && $scope.chShow==false && $scope.ciShow==false && $scope.cdShow==false && $scope.crShow==false){
					$rootScope.showMF();
				}
				$scope.musica_funcional = 1;
				$('body').removeClass('hide');
				$scope.cont++;
			}			
		}

		
		$rootScope.primeravez=true;
		if($scope.cont==0){
			url = encodeURI(url+'?ruta=config/licencia');
			// window.location = '../../demo/?protocolo=http&recurso='+url+'&w=1920&h=1080&black=on';
			window.location.replace(url);
			// alert($scope.cont);
		}
	})

	$interval(function(){
		$http.get(url+'?ruta=api/licencia_info&finger='+fingerprint[1]).then(function(data){
			console.log('165',data.data);
			var licencias = data.data.licencia;
			$scope.cont=0;
			for(var i=0;i<licencias.length;i++){
				
				if(licencias[i].product_id==6 && licencias[i].status=='Active' && licencias[i].descarga==1){
					if($rootScope.csAct==false){
						$rootScope.showCS();
					}
					$rootScope.csAct=true;
					
					$('body').removeClass('hide');
					$scope.cont++;
				}
				if(licencias[i].product_id==5 && licencias[i].status=='Active' && licencias[i].descarga==1){
					if($rootScope.primeravez==false && $rootScope.csAct==false){
						$rootScope.showCH();
					}
					$scope.cartelera_homenaje = 1;
					$('body').removeClass('hide');
					$scope.cont++;
				}
				if(licencias[i].product_id==9 && licencias[i].status=='Active' && licencias[i].descarga==1){
					if($rootScope.primeravez==false && $rootScope.csAct==false && $scope.chShow==false){
						$rootScope.showCI();
					}
					$scope.cartelera_informacion = 1;
					$('body').removeClass('hide');
					$scope.cont++;
				}
				if(licencias[i].product_id==33 && licencias[i].status=='Active' && licencias[i].descarga==1){
					if($rootScope.primeravez==false && $rootScope.csAct==false && $scope.chShow==false && $scope.ciShow==false){
						$rootScope.showCD();
					}
					$scope.catalogo_digital = 1;
					$('body').removeClass('hide');
					$scope.cont++;
				}
				if(licencias[i].product_id==32 && licencias[i].status=='Active' && licencias[i].descarga==1){
					if($rootScope.primeravez==false && $rootScope.csAct==false && $scope.chShow==false && $scope.ciShow==false && $scope.cdShow==false){
						$rootScope.showCR();
					}
					$scope.cartelera_control = 1;
					$('body').removeClass('hide');
					$scope.cont++;
				}
				if(licencias[i].product_id==13 && licencias[i].status=='Active' && licencias[i].descarga==1){
					if($rootScope.primeravez==false && $rootScope.csAct==false && $scope.chShow==false && $scope.ciShow==false && $scope.cdShow==false && $scope.crShow==false){
						$rootScope.showMF();
					}
					$scope.musica_funcional = 1;
					$('body').removeClass('hide');
					$scope.cont++;
				}				
			}
			$rootScope.primeravez=true;
			if($scope.cont==0){
				url = encodeURI(url+'?ruta=config/licencia');
				// window.location = 'http://localhost:8080/demo/?protocolo=http&recurso='+url+'&w=1920&h=1080&black=on';
				window.location.replace(url);
				// alert($scope.cont);
			}
			if (data.data.sesion.id!==$scope.sesion.id){
				$scope.license_alert = true;
				$('#center').remove();
				$('#license_alert').append('<center id="center"><div style="margin-top: 25%;font-size: 49px;">se abrio la aplicación en otro dispositivo...</div></center>')


			}
		})
	},15000);
	
	$interval(function(){
		$http.get(url+'?ruta=api/config/ch').then(function(data){
			$scope.configCH = data.data.configCH;
			if($scope.configCH.tipo_servidor == 'web'){
				
				$http.get(url+'?ruta=config/ch/sync/servicios&url='+$scope.configCH.servidor_web).then(function(data){
					
				},function(err){
					
				});
			}		
		});
	},120000);
	
	$scope.CurrentDate = new Date();
	$scope.Hora = ($scope.CurrentDate.getHours()<10?"0":"")+$scope.CurrentDate.getHours()+':'+ ($scope.CurrentDate.getMinutes()<10?'0':'') + $scope.CurrentDate.getMinutes()+':'+ ($scope.CurrentDate.getSeconds()<10?'0':'') + $scope.CurrentDate.getSeconds();
	$scope.FechaHoy = $scope.CurrentDate.getFullYear()+"-"+($scope.CurrentDate.getMonth()+1)+"-"+(($scope.CurrentDate.getDate())<10?'0':'')+($scope.CurrentDate.getDate());
	$scope.FechaManana = $scope.CurrentDate.getFullYear()+"-"+($scope.CurrentDate.getMonth()+1)+"-"+(($scope.CurrentDate.getDate()+1)<10?'0':'')+($scope.CurrentDate.getDate()+1);
 

	$http.get(url+'?ruta=api/config/ch').then(function(data){
		$scope.configCH = data.data.configCH;
		

// 		$rootScope.showCS();

		$scope.url_api_cr=data.data.configCH.url_api_cr + $scope.idsala;
		$scope.url_api_mf=data.data.configCH.url_api_mf;
		$scope.tiempo_cr=data.data.configCH.tiempo_cr * 1000;
		
		if(idsala && idsala[1]>0){
			
			$scope.id_sala = idsala[1];
		}else{
			

			if (typeof $rootScope.fsala_id === 'undefined'){
					$scope.id_sala = $scope.configCH.sala_id;
				}else{
					
					
					$scope.id_sala=$rootScope.fsala_id;
				
				}
		}

		if($scope.configCH.tipo_servidor == 'local'){
			$scope.urlConf = 'http://'+$scope.configCH.servidor_ip+'/cartelera_backend_laravel/public/';
		}else if($scope.configCH.tipo_servidor == 'online'){
			$scope.urlConf = $scope.configCH.servidor_url+'cartelera_backend_laravel/public/';
		}else{
			$scope.urlConf = url;
		}
		

		$http.get($scope.urlConf+'?ruta=api/servicio&id=' + $scope.id_sala  ).then(function(data){
			$scope.servicio = data.data;
			

			if(!$scope.servicio.id){
				$scope.public_act = true;
			}else{
				$scope.public_act = false;
				$http.get(url+'?ruta=api/config/ni').then(function(res){
				$scope.configNI = res.data.configNI;

				// $rootScope.color_fondo =$scope.configNI.color_back;
				if($scope.configNI.color_back.substring(0,1) == '#') {
				 	var color = $scope.configNI.color_back.substring(1);
				}
				r = parseInt(color.substring(0,2),16);
				g = parseInt(color.substring(2,4),16);
				b = parseInt(color.substring(4),16);

				$rootScope.color_fondo ="rgba("+r+","+g+","+b+",0.6)";

				var color_cont = chroma($scope.configNI.color_back).darken().hex();
				if(color_cont.substring(0,1) == '#') {
				 var color2 = color_cont.substring(1);
				}
				r1 = parseInt(color2.substring(0,2),16);
				g1 = parseInt(color2.substring(2,4),16);
				b1 = parseInt(color2.substring(4),16);

				$rootScope.color_cont ="rgba("+r1+","+g1+","+b1+",0.6)";

				// $rootScope.color_cont = chroma($rootScope.color_fondo).darken().hex();

				$scope.url_logo_pie = $scope.urlConf + 'images/empresa/'+$scope.configNI.logo_pie;

				if ($scope.configNI.tipo_qrcode == 'QR1'){
					$scope.qrcode = $scope.urlConf + 'images/servicios/qrcodes/' + data.data.qrcode1;
					// $scope.url_web_homenaje = 'www.'+data.data.url1.substring(7,50);

					var str = data.data.url1; 
					var http = str.slice(0, 7);
					var www = str.slice(7, 11);
					var rest = str.slice(11);
					var rest2 = str.slice(7);
					var https = str.slice(0, 8);
					var www2 = str.slice(8, 12);
					var rest3 = str.slice(12);
					var rest4 = str.slice(8);
					var www3 = str.slice(0,4);
					var rest5 = str.slice(4);

					if(http == 'http://'){
						if(www == 'www.'){
							$scope.url_web_homenaje = www+rest;
						}else{
							$scope.url_web_homenaje = 'www.'+rest2;
						}
					}else if(https == 'https://'){
						if(www2 == 'www.'){
							$scope.url_web_homenaje = www2+rest3;
						}else{
							$scope.url_web_homenaje = 'www.'+rest4;
						}
					}else if(www3 == 'www.'){
						$scope.url_web_homenaje = str;
					}else{
						$scope.url_web_homenaje = $scope.servicios.url1;
					}

				}else if($scope.configNI.tipo_qrcode == 'QR2'){
					$scope.qrcode = url + 'images/ni/qrcodes/' + $scope.configNI.qrcode;
					// $scope.url_web_homenaje = 'www.'+data.data.url2.substring(7,50);

					var str = $scope.configNI.url_web; 
					var http = str.slice(0, 7);
					var www = str.slice(7, 11);
					var rest = str.slice(11);
					var rest2 = str.slice(7);
					var https = str.slice(0, 8);
					var www2 = str.slice(8, 12);
					var rest3 = str.slice(12);
					var rest4 = str.slice(8);
					var www3 = str.slice(0,4);
					var rest5 = str.slice(4);

					if(http == 'http://'){
						if(www == 'www.'){
							$scope.url_web_homenaje = www+rest;
						}else{
							$scope.url_web_homenaje = 'www.'+rest2;
						}
					}else if(https == 'https://'){
						if(www2 == 'www.'){
							$scope.url_web_homenaje = www2+rest3;
						}else{
							$scope.url_web_homenaje = 'www.'+rest4;
						}
					}else if(www3 == 'www.'){
						$scope.url_web_homenaje = str;
					}else{
						$scope.url_web_homenaje = $scope.configNI.url_web;
					}
				}
			});
			}
			    				
			
		});
	});

	$interval(function () {
		$http.get(url+'?ruta=api/config/ch').then(function(data){
			$scope.configCH = data.data.configCH;
			
			if(idsala && idsala[1]>0){
				
				$scope.id_sala = idsala[1];
			}else{
				if (typeof $rootScope.fsala_id === 'undefined'){
					
					$scope.id_sala = $scope.configCH.sala_id;
				}else{
					
					
					$scope.id_sala=$rootScope.fsala_id;
				
				}
			}

			if($scope.configCH.tipo_servidor == 'local'){
				$scope.urlConf = 'http://'+$scope.configCH.servidor_ip+'/cartelera_backend_laravel/public/';
			}else if($scope.configCH.tipo_servidor == 'online'){
				$scope.urlConf = $scope.configCH.servidor_url+'cartelera_backend_laravel/public/';
			}else{
				$scope.urlConf = url;
			}
			

			$http.get($scope.urlConf+'?ruta=api/servicio&id=' + $scope.id_sala  ).then(function(data){
				$scope.servicio = data.data;
				

				if(!$scope.servicio.id){
					$scope.public_act = true;
				}else{
					$scope.public_act = false;
					$http.get(url+'?ruta=api/config/ni').then(function(res){
					$scope.configNI = res.data.configNI;

					// $rootScope.color_fondo =$scope.configNI.color_back;
					if($scope.configNI.color_back.substring(0,1) == '#') {
					 	var color = $scope.configNI.color_back.substring(1);
					}
					r = parseInt(color.substring(0,2),16);
					g = parseInt(color.substring(2,4),16);
					b = parseInt(color.substring(4),16);

					$rootScope.color_fondo ="rgba("+r+","+g+","+b+",0.6)";

					var color_cont = chroma($scope.configNI.color_back).darken().hex();
					if(color_cont.substring(0,1) == '#') {
					 var color2 = color_cont.substring(1);
					}
					r1 = parseInt(color2.substring(0,2),16);
					g1 = parseInt(color2.substring(2,4),16);
					b1 = parseInt(color2.substring(4),16);

					$rootScope.color_cont ="rgba("+r1+","+g1+","+b1+",0.6)";
					// $rootScope.color_cont = chroma($rootScope.color_fondo).darken().hex();

					$scope.url_logo_pie = $scope.urlConf + 'images/empresa/'+$scope.configNI.logo_pie;

					if ($scope.configNI.tipo_qrcode == 'QR1'){
						$scope.qrcode = $scope.urlConf + 'images/servicios/qrcodes/' + data.data.qrcode1;
						
						var str = data.data.url1; 
						var http = str.slice(0, 7);
						var www = str.slice(7, 11);
						var rest = str.slice(11);
						var rest2 = str.slice(7);
						var https = str.slice(0, 8);
						var www2 = str.slice(8, 12);
						var rest3 = str.slice(12);
						var rest4 = str.slice(8);
						var www3 = str.slice(0,4);
						var rest5 = str.slice(4);

						if(http == 'http://'){
							if(www == 'www.'){
								$scope.url_web_homenaje = www+rest;
							}else{
								$scope.url_web_homenaje = 'www.'+rest2;
							}
						}else if(https == 'https://'){
							if(www2 == 'www.'){
								$scope.url_web_homenaje = www2+rest3;
							}else{
								$scope.url_web_homenaje = 'www.'+rest4;
							}
						}else if(www3 == 'www.'){
							$scope.url_web_homenaje = str;
						}else{
							$scope.url_web_homenaje = $scope.servicios.url1;
						}

					}else if($scope.configNI.tipo_qrcode == 'QR2'){
						$scope.qrcode = url + 'images/ni/qrcodes/' + $scope.configNI.qrcode;
						
						var str = $scope.configNI.url_web; 
						var http = str.slice(0, 7);
						var www = str.slice(7, 11);
						var rest = str.slice(11);
						var rest2 = str.slice(7);
						var https = str.slice(0, 8);
						var www2 = str.slice(8, 12);
						var rest3 = str.slice(12);
						var rest4 = str.slice(8);
						var www3 = str.slice(0,4);
						var rest5 = str.slice(4);

						if(http == 'http://'){
							if(www == 'www.'){
								$scope.url_web_homenaje = www+rest;
							}else{
								$scope.url_web_homenaje = 'www.'+rest2;
							}
						}else if(https == 'https://'){
							if(www2 == 'www.'){
								$scope.url_web_homenaje = www2+rest3;
							}else{
								$scope.url_web_homenaje = 'www.'+rest4;
							}
						}else if(www3 == 'www.'){
							$scope.url_web_homenaje = str;
						}else{
							$scope.url_web_homenaje = $scope.configNI.url_web;
						}
					}
				});
				}
				
			});
			
		});


	}, 15000);

    $rootScope.showCH= function(){
               
        if ($scope.chShow!=true){
        	$scope.ciShow = false;
        	$scope.cdShow = false;
        	$scope.mfShow = false;
        	$scope.crShow = false;
        	$scope.csShow=false;
        	$scope.showDiv = {};
        	$scope.chShow = true;
        	$scope.chStyle = {'background':$rootScope.color_cont}
        	$scope.ciStyle = {'background':$rootScope.color_fondo}
        	$scope.csStyle = {'background':$rootScope.color_fondo}
        	$scope.cdStyle = {'background':$rootScope.color_fondo}
        	$scope.mfStyle = {'background':$rootScope.color_fondo}
        	$scope.crStyle = {'background':$rootScope.color_fondo}
	        
		        
	        if($rootScope.csAct==true){
	        	$timeout.cancel($scope.ciTime);
		        $timeout.cancel($scope.mfTime);
		        $timeout.cancel($scope.cdTime);
		        $timeout.cancel($scope.crTime);

	        	$scope.chTime = $timeout(function () {	        	
		            $scope.chShow = false;
		            $rootScope.showCS();
		        }, 60000);
	        }		        
	        
	    }
    }
    $rootScope.showCI= function(){
               
        if ($scope.ciShow!=true){
        	$scope.chShow = false;
        	$scope.showDiv = {height: 0, overflow: "hidden"};
        	$scope.cdShow = false;
        	$scope.mfShow = false;
        	$scope.crShow = false;
        	$scope.csShow=false;
        	$scope.ciShow = true;
        	$scope.chStyle = {'background':$rootScope.color_fondo}
        	$scope.ciStyle = {'background':$rootScope.color_cont}
        	$scope.cdStyle = {'background':$rootScope.color_fondo}
        	$scope.mfStyle = {'background':$rootScope.color_fondo}
        	$scope.crStyle = {'background':$rootScope.color_fondo}
        	$scope.csStyle = {'background':$rootScope.color_fondo}

	        	
	        if($rootScope.csAct==true){
	        	$timeout.cancel($scope.chTime);
		        $timeout.cancel($scope.mfTime);
		        $timeout.cancel($scope.cdTime);
		        $timeout.cancel($scope.crTime);

	        	$scope.ciTime = $timeout(function () {	        	
		            $scope.ciShow = false;
		            $rootScope.showCS();
		        }, 60000);
	        }
		        
	    }
    }
    $rootScope.showCD= function(){
               
        if ($scope.cdShow!=true){
        	$scope.chShow = false;
        	$scope.showDiv = {height: 0, overflow: "hidden"};
        	$scope.ciShow = false;
        	$scope.mfShow = false;
        	$scope.crShow = false;
        	$scope.csShow=false;
        	$scope.cdShow = true;
        	$scope.chStyle = {'background':$rootScope.color_fondo}
        	$scope.ciStyle = {'background':$rootScope.color_fondo}
        	$scope.cdStyle = {'background':$rootScope.color_cont}
        	$scope.mfStyle = {'background':$rootScope.color_fondo}
        	$scope.crStyle = {'background':$rootScope.color_fondo}
        	$scope.csStyle = {'background':$rootScope.color_fondo}

	        	
	        if($rootScope.csAct==true){
	        	$timeout.cancel($scope.ciTime);
		        $timeout.cancel($scope.mfTime);
		        $timeout.cancel($scope.chTime);
		        $timeout.cancel($scope.crTime);

		        $scope.cdTime = $timeout(function () {	        	
		            $scope.cdShow = false;
		            $rootScope.showCS();
		        }, 60000);
	        }
		        
	    }
    }
    $rootScope.showMF= function(){
               
        if ($scope.mfShow!=true){
        	$scope.chShow = false;
        	$scope.showDiv = {height: 0, overflow: "hidden"};
        	$scope.ciShow = false;
        	$scope.cdShow = false;
        	$scope.crShow = false;
        	$scope.csShow=false;
        	$scope.mfShow = true;
        	$scope.chStyle = {'background':$rootScope.color_fondo}
        	$scope.ciStyle = {'background':$rootScope.color_fondo}
        	$scope.cdStyle = {'background':$rootScope.color_fondo}
        	$scope.mfStyle = {'background':$rootScope.color_cont}
        	$scope.crStyle = {'background':$rootScope.color_fondo}
        	$scope.csStyle = {'background':$rootScope.color_fondo}

        	if($rootScope.csAct==true){
        		$timeout.cancel($scope.ciTime);
		        $timeout.cancel($scope.chTime);
		        $timeout.cancel($scope.cdTime);
		        $timeout.cancel($scope.crTime);

		        $scope.mfTime = $timeout(function () {	        	
		            $scope.mfShow = false;
		            $rootScope.showCS();
		        }, 60000);
        	}
	        	
	    }
    }
    $rootScope.showCR= function(){       
        
        if ($scope.crShow!=true){
        	$scope.chShow = false;
        	$scope.showDiv = {height: 0, overflow: "hidden"};
        	$scope.ciShow = false;
        	$scope.cdShow = false;
        	$scope.mfShow = false;
        	$scope.csShow=false;
        	$scope.crShow = true;
        	$scope.chStyle = {'background':$rootScope.color_fondo}
        	$scope.ciStyle = {'background':$rootScope.color_fondo}
        	$scope.cdStyle = {'background':$rootScope.color_fondo}
        	$scope.mfStyle = {'background':$rootScope.color_fondo}
        	$scope.crStyle = {'background':$rootScope.color_cont}
        	$scope.csStyle = {'background':$rootScope.color_fondo}

        	if($rootScope.csAct==true){
        		$timeout.cancel($scope.ciTime);
		        $timeout.cancel($scope.mfTime);
		        $timeout.cancel($scope.cdTime);
		        $timeout.cancel($scope.chTime);

		        $scope.crTime = $timeout(function () {	        	
		            $scope.crShow = false;
		            $rootScope.showCS();
		        }, 60000);
        	}
	        	
	    }
    }
    $rootScope.showCS= function(){
               
        if ($scope.csShow!=true){
        	$scope.chShow = false;
        	$scope.showDiv = {height: 0, overflow: "hidden"};
        	$scope.ciShow = false;
        	$scope.cdShow = false;
        	$scope.mfShow = false;
        	$scope.csShow=true;
        	$scope.crShow = false;
        	$scope.chStyle = {'background':$rootScope.color_fondo}
        	$scope.ciStyle = {'background':$rootScope.color_fondo}
        	$scope.cdStyle = {'background':$rootScope.color_fondo}
        	$scope.mfStyle = {'background':$rootScope.color_fondo}
        	$scope.crStyle = {'background':$rootScope.color_fondo}
        	$scope.csStyle = {'background':$rootScope.color_cont}
	        // $timeout(function () {
	        	
	        //     $scope.chShow = false;
	        // }, 10000);
	    }
    }


});

app.controller('publicCtrl', ['$scope','$interval','$rootScope','$http', function($scope,$interval,$rootScope,$http){
	
	$rootScope.intervalbanner = 2000;
	$scope.url = url;
	$scope.onInit = function (swiper) {
    	$scope.swiper = swiper;    	
    	$scope.swiper.params.autoplay = $rootScope.intervalbanner;
	};

	$http.get(url+'?ruta=api/config/ni').then(function(data){

		$rootScope.intervalbanner = data.data.configNI.duracion_publicidad * 1000;

		var publis = data.data.publis;

		
		$scope.publicidad = angular.fromJson($scope.publis);
	
	});

	$interval(function(){
		$http.get(url+'?ruta=api/config/ni').then(function(data){
			

			$rootScope.intervalbanner = data.data.configNI.duracion_publicidad * 1000;

			slideLength = $scope.swiper.slidesSizesGrid.length;

			var publisNew = data.data.publis;
			
			
			if(publisNew != $scope.publicidad){
				
				$scope.swiper.stopAutoplay();
				$scope.publicidad = angular.fromJson(publisNew);
				$scope.swiper.update();
				$scope.swiper.startAutoplay();	
			}	
		});
	},10000);
}])
